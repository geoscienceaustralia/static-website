#!/bin/bash

export TF_VAR_bucket_name=$1

echo "Deploying terraform"
terraform init
terraform get
terraform plan
terraform apply -auto-approve

echo "Running tests"
cd tests
bundle install
bundle exec rake spec
cd ../