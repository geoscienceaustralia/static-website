#!/bin/bash

export TF_VAR_bucket_name=$1

echo "Destroying Stack"
terraform init
terraform get
terraform destroy -force
