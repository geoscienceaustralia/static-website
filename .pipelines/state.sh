#!/bin/bash

export TF_VAR_bucket_name=$1

echo "Checking terraform state"
terraform init
terraform get
terraform plan