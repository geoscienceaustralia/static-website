#==============================================================
# outputs.tf
#==============================================================

# This file defines the information we want to write to the
# console after a succesful `terraform apply`

output "domain_name" {
  value       = "http://${module.cloudfront_distribution.domain_name}"
  description = "The address of our cloudfront site"
}

