#==============================================================
# variables.tf
#==============================================================

# This file is used to set variables that are passed to sub 
# modules to build our stack

#--------------------------------------------------------------
# Terraform Remote State
#--------------------------------------------------------------

# Define the remote objects that terraform will use to store
# state. We use a remote store, so that you can run destroy
# from a seperate machine to the one it was built on.

terraform {
  required_version = ">= 0.12"
 
  backend "s3" {
    # This is an s3bucket you will need to create in your aws 
    # space
    bucket = "ga-tf-state"

    # The key should be unique to each stack, because we want to
    # have multiple enviornments alongside each other we set
    # this dynamically in the bitbucket-pipelines.yml with the
    # --backend
    key = "s3cfwww/"

    region = "ap-southeast-2"

    # This is a DynamoDB table with the Primary Key set to LockID
    dynamodb_table = "terraform-lock"
  }
}

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Variables used in the global config

variable "region" {
  default     = "ap-southeast-2"
  description = "The region you want to deploy in"
}

variable "owner" {
  default     = "cloudenablement@ga.gov.au"
  description = "The owner email, this will be applied as a tag to objects"
}

#--------------------------------------------------------------
# Cloudfront configuration
#--------------------------------------------------------------

# Variables used to create our cloudfront distribution

# This allows us to view the aws account for aws_caller_identities
# that already exist
data "aws_caller_identity" "current" {
}

# This is the name we will set for the s3 bucket (must be unique)
variable "bucket_name" {
  default     = "ga-tf-s3cfexample"
  description = "The name of the s3 bucket to deploy"
}

