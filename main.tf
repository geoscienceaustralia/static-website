#==============================================================
# main.tf
#==============================================================

# This file is used to configure global settings and create our
# stack by calling individual modules.
# This file is not executed in order, if a module relies on
# the outputs of another terraform will build it in that order.

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Configure the cloud provider

provider "aws" {
  region = var.region
}

#--------------------------------------------------------------
# cloudfront distribution
#--------------------------------------------------------------

# Creates a cloudfront distrubution using the s3 bucket as a
# filestore.

module "cloudfront_distribution" {
  source      = "./modules/cloudfront"
  bucket_name = var.bucket_name
  owner       = var.owner
}

#--------------------------------------------------------------
# s3 Bucket
#--------------------------------------------------------------

# Creates a place to store our files, and uploads the index.html
# files, so our site will have something to host.

module "s3_bucket" {
  source      = "./modules/s3"
  bucket_name = var.bucket_name
  owner       = var.owner
  origin_arn  = module.cloudfront_distribution.origin_arn
}

