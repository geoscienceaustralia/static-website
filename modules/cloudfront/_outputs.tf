#==============================================================
# Cloudfront / outputs.tf
#==============================================================

output "domain_name" {
  value = aws_cloudfront_distribution.www.domain_name
}

output "origin_arn" {
  value = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
}
