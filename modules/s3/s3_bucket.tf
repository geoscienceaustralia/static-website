#==============================================================
# App / variables.tf
#==============================================================

#--------------------------------------------------------------
# S3 Bucket Policy
#--------------------------------------------------------------

# Allow access from cloudfront to the bucket.

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    principals {
      type        = "AWS"
      identifiers = [var.origin_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["arn:aws:s3:::${var.bucket_name}"]

    principals {
      type        = "AWS"
      identifiers = [var.origin_arn]
    }
  }
}

#--------------------------------------------------------------
# S3 Bucket
#--------------------------------------------------------------

# Create a new s3 bucket to host our web server files

resource "aws_s3_bucket" "static_www" {
  bucket = var.bucket_name
  acl    = "private"
  
  force_destroy = true

  policy = data.aws_iam_policy_document.s3_policy.json

  # Enable logging, send the logs to our 'log' bucket
  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "log/"
  }

  tags = {
    owner = var.owner
  }
}

#--------------------------------------------------------------
# S3 Bucket - Logging
#--------------------------------------------------------------

# Create a new s3 bucket to host our logs

# Logs in this bucket will be transitioned to glacier after 30
# days to keep costs down.

# After 90 days logs will be deleted.

resource "aws_s3_bucket" "log_bucket" {
  bucket = "${var.bucket_name}-logs"
  acl    = "log-delivery-write"

  lifecycle_rule {
    id      = "log"
    prefix  = "log/"
    enabled = true

    transition {
      days          = 30
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
  }

  tags = {
    owner = var.owner
  }
}

#--------------------------------------------------------------
# Web files
#--------------------------------------------------------------

# We load index.html into the bucket so our site has something
# to display

resource "aws_s3_bucket_object" "index" {
  bucket = aws_s3_bucket.static_www.id
  key    = "index.html"

  # this is being run from main.tf
  source       = "./modules/s3/index.html"
  content_type = "text/html"
}
